from typing import Optional

from fastapi import FastAPI
from fastapi import Body
from pydantic import BaseModel

app = FastAPI()


class Post(BaseModel):
    title: str
    description: str
    publish: bool = True
    rating: Optional[int] = None


@app.get('/hello')
def index():
    return {'message': 'Hello world!'}


@app.post("/createpost")
def create_post(post: Post):
    print(post)
    print(post.dict())
    return {"message": "send data successfully"}
